package br.com.cp.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cp.api.dto.PessoaDto;
import br.com.cp.api.entities.Pessoa;
import br.com.cp.api.response.Response;
import br.com.cp.api.services.PessoaService;
import br.com.cp.api.utils.PasswordUtils;

@RestController
@RequestMapping("/api/pessoas")
@CrossOrigin(origins = "*")
public class PessoaController {
	
	private static final Logger log = LoggerFactory.getLogger(Pessoa.class);

	@Autowired
	private PessoaService pessoaService;

	public PessoaController() {
	}
	
	@PostMapping
	public ResponseEntity<Response<PessoaDto>> cadastrar(@Valid @RequestBody PessoaDto pessoaDto, BindingResult result)
		throws NoSuchAlgorithmException {
		log.info("Cadastrando Pessoa: {}", pessoaDto.toString());
		Response<PessoaDto> response = new Response<>();
		
		validarDadosExistentes(pessoaDto, result);
		
		if (result.hasErrors()) {
			log.info("Erro validando dados de Pessoa: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		Pessoa pessoa = this.converterDtoParaPessoa(pessoaDto);
		
		this.pessoaService.persistir(pessoa);
		response.setData(this.converterPessoaDto(pessoa));
		
		return ResponseEntity.ok(response);
	}
	
	
	/**
	 * Atualiza os dados de uma pessoa.
	 * 
	 * @param id
	 * @param pessoaDto
	 * @param result
	 * @return ResponseEntity<Response<PessoaDto>>
	 * @throws NoSuchAlgorithmException
	 */
	@PutMapping(value = "/{id}")//usa-se put para atualização
	public ResponseEntity<Response<PessoaDto>> atualizar(@PathVariable("id") Long id,
			@Valid @RequestBody PessoaDto pessoaDto, BindingResult result) throws NoSuchAlgorithmException {
		log.info("Atualizando Pessoa: {}", pessoaDto.toString());
		Response<PessoaDto> response = new Response<PessoaDto>();
		
		Optional<Pessoa> pessoa = this.pessoaService.buscarPorId(id);
		if (!pessoa.isPresent()) {
			result.addError(new ObjectError("pessoa", "Pessoa não encontrada."));
		}
		this.atualizarDadosDePesoa(pessoa.get(), pessoaDto, result);
		
		if(result.hasErrors()){
			log.error("Erro validando pessoa: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		this.pessoaService.persistir(pessoa.get());
		response.setData(this.converterPessoaDto(pessoa.get()));
		
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Atualiza dados de pessoa com base nos dados encontrados no DTO
	 * @param pessoa
	 * @param pessoaDto
	 * @param result
	 * @throws NoSuchAlgorithmException
	 */
	public void atualizarDadosDePesoa(Pessoa pessoa, PessoaDto pessoaDto, BindingResult result) throws NoSuchAlgorithmException {
		
		if(pessoaDto.getNome() != null) {
			pessoa.setNome(pessoaDto.getNome());
		}
		if (pessoaDto.getSenha().isPresent()) {
			pessoa.setSenha(PasswordUtils.gerarBCrypt(pessoaDto.getSenha().get()));
		}
		if(!pessoa.getEmail().equals(pessoaDto.getEmail())) {
			this.pessoaService.buscarPorEmail(pessoaDto.getEmail()).ifPresent(func -> result.addError(new ObjectError("Email", "E-mail já existente.")));
			//pessoa.setEmail(pessoaDto.getEmail());
		}
		if(!pessoa.getLogin().equals(pessoaDto.getLogin())) {
			this.pessoaService.buscarPorLogin(pessoaDto.getLogin()).ifPresent(func -> result.addError(new ObjectError("Login", "Login já existente.")));
			//pessoa.setLogin(pessoaDto.getLogin());
		}
	}
	
	private void validarDadosExistentes(PessoaDto pessoaDto, BindingResult result) {
		this.pessoaService.buscarPorCpf(pessoaDto.getCpf())
			.ifPresent(pes -> result.addError(new ObjectError("pessoa", "CPF existente.")));
		this.pessoaService.buscarPorEmail(pessoaDto.getEmail())
			.ifPresent(pes -> result.addError(new ObjectError("pessoa", "E-mail já existente.")));
		this.pessoaService.buscarPorLogin(pessoaDto.getLogin())
			.ifPresent(pes -> result.addError(new ObjectError("pessoa", "Login já cadastrado.")));
	}	
	
	private Pessoa converterDtoParaPessoa(PessoaDto pessoaDto) {
		Pessoa pessoa = new Pessoa();
		pessoa.setCpf(pessoaDto.getCpf());
		pessoa.setEmail(pessoaDto.getEmail());
		pessoa.setLogin(pessoaDto.getLogin());
		pessoa.setNome(pessoaDto.getNome());
		pessoaDto.getSenha().ifPresent(senha -> pessoa.setSenha(senha));
		return pessoa;
	}
	
	private PessoaDto converterPessoaDto(Pessoa pessoa) {
		PessoaDto pessoaDto = new PessoaDto();
		pessoaDto.setId(pessoa.getId());
		pessoaDto.setEmail(pessoa.getEmail());
		pessoaDto.setCpf(pessoa.getCpf());
		pessoaDto.setLogin(pessoa.getLogin());
		pessoaDto.setNome(pessoa.getNome());
		pessoa.getSenhaOpt().ifPresent(senhaUser -> pessoaDto.setSenha(Optional.empty()));
		return pessoaDto;
	}
	
}
