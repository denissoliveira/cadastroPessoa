package br.com.cp.api.dto;

import java.util.Optional;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

public class PessoaDto {

	private Long id;
	private String nome;
	private String cpf;
	private String email;
	private Optional<String>  senha = Optional.empty();
	private String login;

	public PessoaDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotEmpty(message = "Nome não pode ser vazio")
	@Length(min = 3, max = 250, message = "Nome deve ter entre 3 a 250 caracteres.")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@NotEmpty(message = "Email não pode ser vazio.")
	@Length(min = 5, max = 200, message = "Email deve conter entre 5 e 200 caracteres.")
	@Email(message = "Email inválido.")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Optional<String> getSenha() {
		return senha;
	}

	public void setSenha(Optional<String> senha) {
		this.senha = senha;
	}

	@NotEmpty(message = "Login não pode ser vazio.")
	@Length(min = 4, max = 50, message = "Login deve conter 4 a 50 digitos/caracteres.")
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@NotEmpty(message = "CPF não pode ser vazio.")
	@Length(min = 11, max = 11, message = "CPF deve ter 11 digitos.")
	@CPF
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return "PessoaDto [id=" + id + ", nome=" + nome + ", email=" + email + ", login=" + login
				+ "]";
	}

}
