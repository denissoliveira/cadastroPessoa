package br.com.cp.api.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.cp.api.enums.GrauParentescoEnum;

@Entity
@Table(name = "dependente")
public class Dependente implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5867182188948261103L;
	
	private Long id;
	private Pessoa instituidor;
	private Pessoa depInstituidor;
	private GrauParentescoEnum grauParentesco;
	private Date dataInclusao;
	private Date dataExclusão;
	
	public Dependente() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dep_generator")
	@SequenceGenerator(name="dep_generator", sequenceName = "dep_seq", allocationSize=1)
	@Column(name = "dependente_id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	public Pessoa getInstituidor() {
		return instituidor;
	}
	public void setInstituidor(Pessoa instituidor) {
		this.instituidor = instituidor;
	}
	
	@OneToOne
	public Pessoa getDepInstituidor() {
		return depInstituidor;
	}
	public void setDepInstituidor(Pessoa depInstituidor) {
		this.depInstituidor = depInstituidor;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "grau_parentesco", nullable = false)
	public GrauParentescoEnum getGrauParentesco() {
		return grauParentesco;
	}
	public void setGrauParentesco(GrauParentescoEnum grauParentesco) {
		this.grauParentesco = grauParentesco;
	}
	
	@Column(name = "data_inclusao", nullable = false)
	public Date getDataInclusao() {
		return dataInclusao;
	}
	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	
	@Column(name = "data_exclusao")
	public Date getDataExclusão() {
		return dataExclusão;
	}
	public void setDataExclusão(Date dataExclusão) {
		this.dataExclusão = dataExclusão;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((depInstituidor == null) ? 0 : depInstituidor.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((instituidor == null) ? 0 : instituidor.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dependente other = (Dependente) obj;
		if (depInstituidor == null) {
			if (other.depInstituidor != null)
				return false;
		} else if (!depInstituidor.equals(other.depInstituidor))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (instituidor == null) {
			if (other.instituidor != null)
				return false;
		} else if (!instituidor.equals(other.instituidor))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Dependente [id=" + id + ", instituidor=" + instituidor + ", depInstituidor=" + depInstituidor
				+ ", grauParentesco=" + grauParentesco + ", dataInclusao=" + dataInclusao + ", dataExclusão="
				+ dataExclusão + "]";
	}
	
}
