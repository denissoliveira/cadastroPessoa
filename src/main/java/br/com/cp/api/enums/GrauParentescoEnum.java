package br.com.cp.api.enums;

public enum GrauParentescoEnum {
	ESPOSA,
	ESPOSO,
	FILHO,
	FILHA,
	PAI,
	MAE,
	IRMAO,
	IRMA,
	TIO,
	TIA;
}
