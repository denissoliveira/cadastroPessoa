package br.com.cp.api.repositories;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.cp.api.entities.Pessoa;

@Transactional(readOnly = true) //somente leitura
@NamedQueries({ // caso spring não faça por convesão
	@NamedQuery(name = "PessoaRepository.findByLogin", query = "SELECT pes FROM Pessoa pes WHERE pes.login = :login") })
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
	
	Pessoa findById(Long id);
	
	Pessoa findByCpf(String cpf);
	
	Pessoa findByEmail(String email);
	
	Pessoa findByCpfOrEmail(String cpf, String email);
	
	//Pessoa findBylogin(String login);
	
	Long countByCpf(String cpf);
	
	//usando namedQuary
	Pessoa findByLogin(@Param("login") String login);

	Page<Pessoa> findByLogin(@Param("login") String login, Pageable pageable);
	
	//Using @Query
	@Query("select p from Pessoa p where p.email = ?1")
	Pessoa pesquisaEnderecoDeEmail(String email);
	
	//Using advanced LIKE expressions
	@Query("select p from Pessoa p where p.nome like %?1%")
	Pessoa pesquisaParteDoNome(String nome);
	
	//Native queries
	@Query(value = "SELECT * FROM pessoa WHERE email = ?1", nativeQuery = true)
	Pessoa pesquisaNativoEmail(String email);
	
	//Using named parameters
	@Query("select p from Pessoa p where p.cpf = :cpf or p.email = :email")
	Pessoa pesquisarPorCpfEEmail(@Param("cpf") String cpf, @Param("email") String email);
	
}
