package br.com.cp.api.services;

import java.util.Optional;

import br.com.cp.api.entities.Pessoa;

public interface PessoaService {
	
	/**
	 * Persistir uma Pessoa
	 * @param {@link Pessoa}
	 * @return {@link Pessoa}
	 */
	Pessoa persistir(Pessoa pessoa);
	
	/**
	 * Remove uma Pessoa
	 * @param {@link Pessoa}
	 */
	void remover(Pessoa pessoa);
	
	/**
	 * Buscar e retornar uma pessoa por id
	 * @param id
	 * @return Optional<Pessoa>
	 */
	Optional<Pessoa> buscarPorId(Long id);
	
	/**
	 * Buscar e retornar uma pessoa por cpf
	 * @param cpf
	 * @return Optional<Pessoa>
	 */
	Optional<Pessoa> buscarPorCpf(String cpf);
	
	/**
	 * Retorna um count de CPF
	 * @param cpf
	 * @return Long
	 */
	Long verificarCPFExistente(String cpf);
	
	/**
	 * Buscar e retornar uma pessoa por e-mail
	 * @param e-mail
	 * @return Optional<Pessoa>
	 */
	Optional<Pessoa> buscarPorEmail(String email);
	
	/**
	 * Buscar e retornar uma pessoa por cpf e/ou e-mail
	 * @param cpf,email
	 * @return Optional<Pessoa>
	 */
	Optional<Pessoa> buscarPorCpfOuEmail(String cpf, String email);
	
	/**
	 * Buscar e retornar uma pessoa pelo login
	 * @param login
	 * @return Optional<Pessoa>
	 */
	Optional<Pessoa> buscarPorLogin(String login);

}
