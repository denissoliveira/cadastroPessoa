package br.com.cp.api.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cp.api.entities.Pessoa;
import br.com.cp.api.repositories.PessoaRepository;
import br.com.cp.api.services.PessoaService;

@Service
public class PessoaServiceImpl implements PessoaService {
	
	private static final Logger log = LoggerFactory.getLogger(PessoaServiceImpl.class);
	
	@Autowired
	private PessoaRepository pessoaRepository;

	@Override
	public Pessoa persistir(Pessoa pessoa) {
		log.info("Persistindo pessoa: {}", pessoa);
		return pessoaRepository.save(pessoa);
	}

	@Override
	public void remover(Pessoa pessoa) {
		log.info("removendo pessoa: {}", pessoa);
		pessoaRepository.delete(pessoa);
	}

	@Override
	public Optional<Pessoa> buscarPorId(Long id) {
		log.info("Buscando pessoa pelo id: {}", id);
		return Optional.ofNullable(this.pessoaRepository.findById(id));
	}

	@Override
	public Optional<Pessoa> buscarPorCpf(String cpf) {
		log.info("Buscando pessoa pelo CPF: {}", cpf);
		return Optional.ofNullable(this.pessoaRepository.findByCpf(cpf));
	}

	@Override
	public Optional<Pessoa> buscarPorEmail(String email) {
		log.info("Buscando pessoa pelo E-mail: {}", email);
		return Optional.ofNullable(this.pessoaRepository.findByEmail(email));
	}

	@Override
	public Optional<Pessoa> buscarPorCpfOuEmail(String cpf, String email) {
		log.info("Buscando pessoa pelo cpf e/ou E-mail: {1}, {2}", cpf,email);
		return Optional.ofNullable(this.pessoaRepository.findByCpfOrEmail(cpf, email));
	}

	@Override
	public Optional<Pessoa> buscarPorLogin(String login) {
		log.info("Buscando pessoa pelo login: {}", login);
		return Optional.ofNullable(this.pessoaRepository.findByLogin(login));
	}

	@Override
	public Long verificarCPFExistente(String cpf) {
		log.info("Buscando CPF existente: {}", cpf);
		return this.pessoaRepository.countByCpf(cpf);
	}

}
