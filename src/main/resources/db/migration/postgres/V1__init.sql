CREATE TABLE public.dependente (
    dependente_id bigint NOT NULL,
    data_exclusao timestamp without time zone,
    data_inclusao timestamp without time zone NOT NULL,
    grau_parentesco character varying(255) NOT NULL,
    dep_instituidor_pessoa_id bigint,
    instituidor_pessoa_id bigint
);

CREATE TABLE public.pessoa (
    pessoa_id bigint NOT NULL,
    cpf character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    idade integer,
    login character varying(255) NOT NULL,
    nome character varying(255) NOT NULL,
    senha character varying(255) NOT NULL
);