package br.com.cp.api.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.cp.api.dto.PessoaDto;
import br.com.cp.api.entities.Pessoa;
import br.com.cp.api.services.PessoaService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PessoaControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private PessoaService pessoaService;
	
	private static final String BASE_URL = "/api/pessoas/";
	private static final Long ID = Long.valueOf(1);
	private static final String EMAIL = "example@email.com";
	private static final String CPF = "68314362344";
	private static final String NOME = "Fulano de Tal";
	private static final String LOGIN = "fulano";
	private static final String SENHA = "senha123";
	
	@Test
	@WithMockUser
	public void testCadastraPessoa() throws Exception {
		Pessoa pessoa = obterDadosDePessoa();
		BDDMockito.given(this.pessoaService.persistir(Mockito.any(Pessoa.class))).willReturn(pessoa);
		BDDMockito.given(this.pessoaService.buscarPorCpf(Mockito.anyString())).willReturn(Optional.empty());
		BDDMockito.given(this.pessoaService.buscarPorEmail(Mockito.anyString())).willReturn(Optional.empty());
		BDDMockito.given(this.pessoaService.buscarPorLogin(Mockito.anyString())).willReturn(Optional.empty());
		
		mvc.perform(MockMvcRequestBuilders.post(BASE_URL)
				.content(this.obterJsonRequisicaoPost())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data.cpf").value(CPF))
				.andExpect(jsonPath("$.data.email").value(EMAIL))
				.andExpect(jsonPath("$.data.login").value(LOGIN))
				.andExpect(jsonPath("$.data.nome").value(NOME))
				.andExpect(jsonPath("$.data.senha").value(SENHA))
				.andExpect(jsonPath("$.errors").isEmpty());
		
	}
	
	private String obterJsonRequisicaoPost() throws JsonProcessingException {
		PessoaDto pessoaDto = new PessoaDto();
		pessoaDto.setId(null);
		pessoaDto.setCpf(CPF);
		pessoaDto.setEmail(EMAIL);
		pessoaDto.setLogin(LOGIN);
		pessoaDto.setNome(NOME);
		pessoaDto.setSenha(Optional.of(SENHA));
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(pessoaDto);
	}
	
	private Pessoa obterDadosDePessoa() {
		Pessoa pessoa = new Pessoa();
		pessoa.setId(ID);
		pessoa.setCpf(CPF);
		pessoa.setEmail(EMAIL);
		pessoa.setLogin(LOGIN);
		pessoa.setNome(NOME);
		pessoa.setSenha(SENHA);
		return pessoa;
	}

}
