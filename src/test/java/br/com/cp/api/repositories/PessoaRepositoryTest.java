package br.com.cp.api.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.cp.api.entities.Pessoa;
import br.com.cp.api.utils.PasswordUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PessoaRepositoryTest {
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	private static final String EMAIL = "example@email.com";
	private static final String CPF = "68314362344";
	
	@Before
	public void setUp() {
		this.pessoaRepository.save(obterDadosPessoa());
	}
	
	@After
	public void tearDown() {
		this.pessoaRepository.deleteAll();
	}
	
	@Test
	public void testeBuscaPessoaPorEmail() {
		Pessoa pessoa = this.pessoaRepository.findByEmail(EMAIL);
		assertEquals(EMAIL, pessoa.getEmail());
	}
	
	@Test
	public void testeBuscarPessoaPorCPF() {
		Pessoa pessoa = this.pessoaRepository.findByCpf(CPF);
		assertEquals(CPF, pessoa.getCpf());
	}
	
	@Test
	public void testeBuscarPessoaPorEmailECPF() {
		Pessoa pessoa = this.pessoaRepository.findByCpfOrEmail(CPF, EMAIL);
		assertNotNull(pessoa);
	}
	
	@Test
	public void testeBuscarPessoaPorEmailECPFInvalido() {
		Pessoa pessoa = this.pessoaRepository.findByCpfOrEmail("99999999999", EMAIL);
		assertNotNull(pessoa);
	}
	
	@Test
	public void testeBuscarPessoaPorEmailInvalidoECPF() {
		Pessoa pessoa = this.pessoaRepository.findByCpfOrEmail(CPF, "email@invalido");
		assertNotNull(pessoa);
	}
	
	@Test
	public void testeBuscarPessoaPorLogin() {
		Pessoa pessoa = this.pessoaRepository.findByLogin("fulano");
		assertNotNull(pessoa);
	}
	
	@Test
	public void testeBuscarListaPessoaVazia() {
		this.pessoaRepository.deleteAll();
		List<Pessoa> listasDePessoas = pessoaRepository.findAll();
		assertTrue(listasDePessoas.isEmpty());
	}
	
	@Test
	public void testeBuscarListaPessoa() {
		testarListaDePessoas();
		List<Pessoa> listasDePessoas = pessoaRepository.findAll();
		assertNotNull(listasDePessoas.get(0));
	}
	
	@Test
	public void testeCpfExistente() {
		Long eCpf = this.pessoaRepository.countByCpf(CPF);
		assertTrue(eCpf > 0);
	}

	
	@Test
	public void testeFindByEnderecoDeEmail() {
		Pessoa pessoa = this.pessoaRepository.pesquisaEnderecoDeEmail(EMAIL);
		assertNotNull(pessoa);
	}
	
	@Test
	public void testeFindByParteDoNome() {
		Pessoa pessoa = this.pessoaRepository.pesquisaParteDoNome("ulano");
		assertNotNull(pessoa);
	}

	@Test
	public void testeFindByNativoEmail() {
		Pessoa pessoa = this.pessoaRepository.pesquisaNativoEmail(EMAIL);
		assertNotNull(pessoa);
	}

	@Test
	public void testePesquisarPorCpfEEmail() {
		Pessoa pessoa = this.pessoaRepository.pesquisarPorCpfEEmail(CPF, EMAIL);
		assertNotNull(pessoa);
	}
	
	
	//Dados para Teste
	private Pessoa obterDadosPessoa() {
		Pessoa pessoa = new Pessoa();
		pessoa.setCpf(CPF);
		pessoa.setEmail(EMAIL);
		pessoa.setLogin("fulano");
		pessoa.setNome("fulano de tal");
		pessoa.setSenha(PasswordUtils.gerarBCrypt("123"));
		return pessoa;
	}
	
	private void testarListaDePessoas() {
		for (int i = 0; i < 10; i++) {
			Pessoa pessoa = new Pessoa();
			pessoa.setCpf("0984967974"+i);
			pessoa.setEmail("email"+i+"@email.com");
			pessoa.setLogin("fulano"+i);
			pessoa.setNome("fulano de tal "+i);
			pessoa.setSenha(PasswordUtils.gerarBCrypt("123"+i));
			this.pessoaRepository.save(pessoa);
			
		}
	}

}
