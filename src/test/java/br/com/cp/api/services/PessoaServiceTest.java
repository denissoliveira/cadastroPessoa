package br.com.cp.api.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.cp.api.entities.Pessoa;
import br.com.cp.api.repositories.PessoaRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PessoaServiceTest {
	
	private static final String EMAIL = "example@email.com";
	private static final String CPF = "68314362344";
	
	@MockBean
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private PessoaService pessoaService;
	
	@Before
	public void setUp() {
		BDDMockito.given(this.pessoaRepository.save(Mockito.any(Pessoa.class))).willReturn(new Pessoa());
		//Não precisa testar remover pq ele retorna void
		BDDMockito.given(this.pessoaRepository.findByCpf(Mockito.anyString())).willReturn(new Pessoa());
		BDDMockito.given(this.pessoaRepository.findByEmail(Mockito.anyString())).willReturn(new Pessoa());
		BDDMockito.given(this.pessoaRepository.findByCpfOrEmail(Mockito.anyString(),Mockito.anyString())).willReturn(new Pessoa());
		BDDMockito.given(this.pessoaRepository.findById(Mockito.anyLong())).willReturn(new Pessoa());
		BDDMockito.given(this.pessoaRepository.findOne(Mockito.anyLong())).willReturn(new Pessoa());
		BDDMockito.given(this.pessoaRepository.findByLogin(Mockito.anyString())).willReturn(new Pessoa());
		//BDDMockito.given(this.pessoaRepository.findAll()).willReturn(new ArrayList<Pessoa>());
	}
	
	@Test
	public void testePersistirPessoa() {
		Pessoa pessoa = this.pessoaService.persistir(new Pessoa());
		assertNotNull(pessoa);
	}
	
	@Test
	public void testeBuscarPorCpf() {
		Optional<Pessoa> pessoa = this.pessoaService.buscarPorCpf(CPF);
		assertTrue(pessoa.isPresent());
	}
	
	@Test
	public void testeBuscarPorEmail() {
		Optional<Pessoa> pessoa = this.pessoaService.buscarPorEmail(EMAIL);
		assertTrue(pessoa.isPresent());
	}
	
	@Test
	public void testeBuscarPorCpfOuEmail() {
		Optional<Pessoa> pessoa = this.pessoaService.buscarPorCpfOuEmail(CPF, EMAIL);
		assertTrue(pessoa.isPresent());
	}
	
	@Test
	public void testeBuscarLogin() {
		Optional<Pessoa> pessoa = this.pessoaService.buscarPorLogin("login");
		assertTrue(pessoa.isPresent());
	}
	
	@Test
	public void testeBuscarPorId() {
		Optional<Pessoa> pessoa = this.pessoaService.buscarPorId(1L);
		assertTrue(pessoa.isPresent());
	}
	
	@Test
	public void testeBuscarPorPessoa() {
		Optional<Pessoa> pessoa = this.pessoaService.buscarPorId(1L);
		assertTrue(pessoa.isPresent());
	}

}
